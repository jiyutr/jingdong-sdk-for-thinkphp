# thinkphp版京东sdk

#### 介绍
对京东官方的sdk进行了简单的修改,使其可以直接在thinkphp框架中使用


#### 安装教程

composer require jiyutr/jingdong-sdk-for-thinkphp

#### 使用说明

可以参考example.php的示例

附京东api文档[https://open.jd.com/home/home#/doc/api?apiCateId=106&apiId=1045&apiName=jingdong.getUserLevel.query]
